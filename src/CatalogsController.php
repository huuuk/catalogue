<?php namespace Huuuk\Catalogue;

// use App\Http\Requests;
// use App\Http\Requests\CatalogRequest;
use App\Http\Controllers\Controller;
use App\Catalog;
// use Validator;

use Illuminate\Http\Request;

class CatalogsController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$types    = Catalog::$types;
		return view('catalogs.index', compact('types'))->withCatalogs(Catalog::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$types = Catalog::$types;
		$type = $request->input('type', null);

		return view('catalogs.create', compact('types', 'type'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CatalogRequest $request)
	{
		$catalog = Catalog::create($request->all());
		if ($request->ajax())
			return $catalog;
		else
			return redirect()->route('catalogs.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$types   = Catalog::$types;
		$catalog = Catalog::findOrFail($id);
		if ($catalog) {
			return view('catalogs.edit', compact('types', 'catalog'));
		}
		else {
			return response(null, 404);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CatalogRequest $request, $id)
	{
		Catalog::findOrFail($id)->update($request->all());
		return redirect()->route('catalogs.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Catalog::destroy($id);
		return redirect()->route('catalogs.index');
	}

	public function trash()
	{
		return view('catalogs.trash');
	}

	public function restore($id)
	{
		Catalog::withTrashed()->where('id', $id)->get()->restore($id);
		return redirect()->back();
	}

	public function forceDelete($id)
	{
		Catalog::withTrashed()->where('id', $id)->get()->forceDelete($id);
		return redirect()->back();
	}

}
