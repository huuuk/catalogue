<?php namespace Huuuk\Catalogue;

use Illuminate\Support\ServiceProvider;

class CatalogueServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->publishes([
			__DIR__.'/views' => base_path('resources/views/catalogs'),
			], 'views');

		$this->publishes([
			__DIR__.'/migrations' => base_path('database/migrations'),
			], 'migrations');

		$this->publishes([
			__DIR__.'/js/scripts.ajaxable.js'  => public_path('js/scripts.ajaxable.js'),
			__DIR__.'/js/scripts.helpers.js'   => public_path('js/scripts.helpers.js'),
			__DIR__.'/js/chosen.jquery.min.js' => public_path('js/vendors/chosen.jquery.min.js'),
			], 'public');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		include __DIR__.'/routes.php';
		$this->app->make('Huuuk\Catalogue\CatalogsController');
	}

}
