(function($) {
	'use strict';

	var checkDependencies = function(){
		// check for all dependencies
   	if (!$.fn.getObjectFromForm) {
   		throw 'Plugin needs "getObjectFromForm" function for correct work';
   	}
   	if (!$.fn.modal) {
   		throw 'Plugin needs "modal" function for correct work';
   	}
   	if (!$.fn.clearForm) {
   		throw 'Plugin needs "clearForm" function for correct work';
   	}
	};

	var showModal = function() {

	};

	var thenCheckAndValidateInput = function(options) {
		if ( typeof options.modal === 'string') {
			options.modal = $(options.modal);
		}
		if (typeof options.types === 'undefined') {
				throw 'You\'ve forgot importent parameter "types"' ;
		}
		return options;
	};

	var findTypeInArray = function(type,types) {
		var returnedType;
		types.forEach(function(value, index) {
			if (value.name === type)
				returnedType = value;
		});
		return returnedType;
	}

	var showModal = function(types, modalTitle, fullNameContainer) {
		return function(e) {
			var form    = $(e.target).find('form'),
					element = $(e.relatedTarget),
					type    = findTypeInArray(element.data('type'), types);
			form.find(modalTitle).html(type.title);
			form.find('input[name=type]').val(type.name);
			if (!type.full_name) {
				form.find(fullNameContainer).hide();
			}
			else {
				form.find(fullNameContainer).show();
			}
		};
	};

	var hideModal = function(e) {
		var form = $(e.target).find('form');
		form.clearForm();
	};

	var saveData = function(object, url, callback) {
		return $.post(url, object, callback);
	};

	var insertInList = function(doItAfterInsert) {
		return function(data) {
			var option = $('<option></option>').text(data.short_name).val(data.id);
			$('select#'+data["type"]+', select.'+data["type"]+', select[name='+data["type"]+']').append(option);
			console.log($('select.'+data.type+', select[name='+data.type+']'));
			if (typeof doItAfterInsert === 'function')
				doItAfterInsert();
		};
	};

	// var store

	$.ajaxable = function(options) {
		checkDependencies();
		var opt = thenCheckAndValidateInput(options);
		opt.modal.on('show.bs.modal', showModal(options.types[0], options.modalTitle, options.fullNameContainer));
		opt.modal.on('hide.bs.modal', hideModal);

		opt.modal.find('form').submit(function(event) {
			var data = $(this).getObjectFromForm();
			saveData(data, options.url, insertInList(options.afterInsertInList))
				.done(function(data) {
					options.modal.modal('hide');
				})
				.fail(options.ajaxFailHandler);
			event.preventDefault();
		});
		// this.click(showModal);
	}
}) (jQuery);