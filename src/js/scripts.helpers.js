$.fn.getObjectFromForm = function() {
	var inputs = this.find('input[type=text], input[type=checkbox]:checked, textarea, select')
		.not(':input[type=button], :input[type=submit], :input[type=reset]'),
		returnedObject = {};
	inputs.each(function(i, item) {
		if (item.name === '' && $(item).val() === '') return;
		returnedObject[item.name] = $(item).val();
	});
	return returnedObject;
};
$.fn.setObjectToForm = function(object, doItAfterFill) {
	var idInput = this.find('input[name="id"]');
	if (idInput.length === 0 && typeof object.id !== 'undefined') {
		this.prepend('<input type="hidden" name="id" />');
	}
	for(prop in object) {
		this.find('[name="' + prop + '"]').val(object[prop]);
	}
	if (typeof doItAfterFill === 'function')
			doItAfterFill();
	return this;
};
$.fn.clearForm = function(doItAfterClear) {
	this.find('input[name="id"]').remove();
	this.find('input[type=checkbox]:checked').attr('checked', false);
	this.find('input, textarea, select')
		.not(':input[type=button], :input[type=submit], :input[type=reset]').val('');
	if (typeof doItAfterClear === 'function')
			doItAfterClear();
	return this;
};

var ajaxFailHandler = function(error) {
  	alert('Ajax call failed \n See Console for more details')
		console.log(error);
	};

$.fn.fillWith = function(object, options) {
		var tr = $(this).attr('data-id', object.id),
			td = $('<td></td>');
		tr.empty();

		// filling tr element with data 
		for (var prop in object) {
			if (options.excludeFields.indexOf(prop) !== -1) 
				continue;
			td.clone().text(object[prop]).appendTo(tr);
		}

		/*// edit cell
		var editCtrl      = options.controls.edit,
				editTdClasses = options.classes.editTd;
		// parsing classes for td elemnt
		if (editTdClasses instanceof Array)
			editTdClasses = editTdClasses.join(' ');
		// parsing edit control
		if (editCtrl instanceof String)
			editCtrl = $(editCtrl);
		else if (editCtrl instanceof Function)
			editCtrl = editCtrl(object);
		td.clone()
			.append(editCtrl)
			.addClass(editTdClasses)
			.appendTo(tr);

		// delete cell
		var deleteCtrl      = options.controls.delete,
				deleteTdClasses = options.classes.deleteTd;
		// parsing classes for td elemnt
		if (deleteTdClasses instanceof Array)
			deleteTdClasses = deleteTdClasses.join(' ');
		// parsing delete control
		if (deleteCtrl instanceof String)
			deleteCtrl = $(deleteCtrl);
		else if (deleteCtrl instanceof Function)
			deleteCtrl = deleteCtrl(object);
		td.clone()
			.append(deleteCtrl)
			.addClass(deleteTdClasses)
				.appendTo(tr);
*/
		return this;
	};