<?php namespace Huuuk\Catalogue;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model {

	public $timestamps = false;

	protected $fillable = ['type', 'short_name', 'full_name'];
}
