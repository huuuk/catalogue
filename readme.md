Catalogue
=========

Простой пакет для бысторого развертывания "Справочников"
###### Содержит
 - Модель "Catalog"
 - Контроллер "CatalogsContorller"
 - Запрос "CatalogRequest"
 - Миграции для создания таблицы в БД
 - 3 предсатвелния (index, create, edit)
 - js файлы (ajaxable, helpers, chosen)

#### Установка
Отредактируйте свой `composer.json`:

    "require": {
        "huuuk/catalogue": "dev-master"
    }

Обновите Composer:

    composer update

Добавьте провайдер в список провайдеров в `config/app.php`:

    'providers' => [
        // ...
        'Huuuk\Catalogue\CatalogueServiceProvider',
        // ...
    ],

Вы можете как идущие в комплекте представления, миграции и js-файлы:

    artisan vendor:publish --povider='Huuuk\Catalogue\CatalogueServiceProvider'

или по отдельности:

    php artisan vendor:publish --povider='Huuuk\Catalogue\CatalogueServiceProvider' --tag=views
    php artisan vendor:publish --povider='Huuuk\Catalogue\CatalogueServiceProvider' --tag=migrations
    php artisan vendor:publish --povider='Huuuk\Catalogue\CatalogueServiceProvider' --tag=public

так и исвпользовать свои собственные. Единственное ограничение, представления должны распологаться в директории `views/catalogs` и именть имена `index.blade.php`, `create.blade.php`, `edit.blade.php`

Создайте модель:

    <?php namespace App;

    class Catalog extends \Huuuk\Catalogue\Catalog {
        // ...
    }

##### Поздравляю, вы установили Catalogue
